package com.test.shop;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class ShopApplicationTests {

    private final String APPLE = "apple";
    private final String ORANGE = "orange";
    @Test
    public void PriceCalculationTest() {
        String[] products = {ORANGE, APPLE, ORANGE};
        Checkout checkout = new Checkout(products);
        assertEquals(1.10d, checkout.calculatePrice(), 0.0001);
    }
    
    @Test
    public void PriceCalculationNonProductTest() {
        String[] products = {"lemon"};
        Checkout checkout = new Checkout(products);
        assertEquals(0.00d, checkout.applyOfferDiscount(), 0.0001);
    }

    @Test
    public void AppleOfferTwoTest() {
        String[] products = {APPLE, APPLE};
        Checkout checkout = new Checkout(products);
        assertEquals(0.60d, checkout.applyOfferDiscount(), 0.0001);
    }
    
    @Test
    public void AppleOfferMultipleTest() {
        String[] products = {APPLE, APPLE, APPLE};
        Checkout checkout = new Checkout(products);
        assertEquals(0.60d, checkout.applyOfferDiscount(), 0.0001);
    }
    
    @Test
    public void OrangeOfferTwoTest() {
        String[] products = {ORANGE, ORANGE};
        Checkout checkout = new Checkout(products);
        assertEquals(0.00d, checkout.applyOfferDiscount(), 0.0001);
    }
    
    @Test
    public void OrangeOfferMultipleTest() {
        String[] products = {ORANGE, ORANGE, ORANGE};
        Checkout checkout = new Checkout(products);
        assertEquals(0.25d, checkout.applyOfferDiscount(), 0.0001);
    }
    
    @Test
    public void ProductOfferMultipleTest() {
        String[] products = {APPLE, ORANGE, ORANGE, APPLE, APPLE, "lemon", ORANGE, ORANGE, APPLE};
        Checkout checkout = new Checkout(products);
        assertEquals(1.45d, checkout.applyOfferDiscount(), 0.0001);
    }
}
