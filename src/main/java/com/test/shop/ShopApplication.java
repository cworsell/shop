package com.test.shop;

public class ShopApplication {

    public static void main(String[] args) {
        if (args.length > 0) {
            Checkout checkout = new Checkout(args);
            System.out.printf("Checkout price: £%.2f\n", checkout.calculatePrice()
                 - checkout.applyOfferDiscount());
        } else {
            System.out.print("Please add some items to your cart.");
        }
    }
}
