package com.test.shop;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Craig Worsell
 */
public class Checkout {
    
    private final static String ORANGE = "ORANGE";
    private final static String APPLE = "APPLE";
    private static final Map<String, Double> AVAILABLE_PRODUCTS = new HashMap<>();
    private String[] orderedProducts;
    
    static {
        AVAILABLE_PRODUCTS.put(APPLE, 0.60d);
        AVAILABLE_PRODUCTS.put(ORANGE, 0.25d);
    }
    
    public Checkout(String... products) {
        this.orderedProducts = products;
    }

    public String[] getOrderedProducts() {
        return orderedProducts;
    }

    public void setOrderedProducts(String[] orderedProducts) {
        this.orderedProducts = orderedProducts;
    }
    
    public double calculatePrice() {
        double price = 0.00d;        
        for (String product:orderedProducts) {
            try {
                price += AVAILABLE_PRODUCTS.get(product.toUpperCase());
            } catch (NullPointerException e) {
                System.out.printf("Unrecognised product %s, no value added\n", product);
            }
        }
        return price;
    }
    
    public double applyOfferDiscount() {
        double discount = 0.00d;
        int appleCount = 0;
        int orangeCount = 0;
        for (String product:orderedProducts) {
            switch (product.toUpperCase()) {
                case APPLE:
                    appleCount++;
                    if (appleCount % 2 == 0) {
                        discount += AVAILABLE_PRODUCTS.get(APPLE);
                    }
                    break;
                case ORANGE:
                    orangeCount++;
                    if (orangeCount % 3 == 0) {
                        discount += AVAILABLE_PRODUCTS.get(ORANGE);
                    }
                    break;
                default:
                    System.out.printf("Unrecognised product %s\n", product);
            }
        }
        return discount;
    }
}
